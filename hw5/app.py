# Joseph Sands - Cloud Systems Spring 2018 - HW2

from flask import Flask, render_template
import flask.views
import requests

from model import DModel

app = Flask(__name__)
model = DModel(app)

@app.route('/')
def index():
  return render_template("index.html")


@app.route('/reviews')
def reviews():
  reviews = model.fetchall()
  return render_template("reviews.html", reviews=reviews)

if __name__ == "__main__":
  app.run(host = '0.0.0.0', port = 8000, debug=True)
