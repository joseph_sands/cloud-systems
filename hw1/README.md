# What I know about markdown:
## After googling markdown for 5 minutes it seems to be a lot like the syntax for commenting on [reddit.com](reddit.com)
I've learned that it supports *italics* as well as **bold text**, turns out reddit was good for something after all.
