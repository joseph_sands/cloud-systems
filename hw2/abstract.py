from abc import ABC, abstractmethod

class BaseModel(ABC):
  
  """
  Fetch all of the entries in the database
  """
  @abstractmethod
  def fetchall(self):
    pass


  """
  Fetch all reviews matching a requested term
  """
  @abstractmethod
  def fetch_by_term(self, term):
    pass


  """
  Fetch all reviews for classes on a certain day
  """
  @abstractmethod
  def fetch_by_instructor(self, instructor):
    pass


  """
  Fetch all reviews matching a rating
  """
  @abstractmethod
  def fetch_by_rating(self, rating):
    pass


  """
  Fetch all reviews with a given ID
  """
  @abstractmethod
  def fetch_by_id(self, id):
    pass


  """
  Add a new review to the database
  """
  @abstractmethod
  def add_review(self, id):
    pass


  """
  Update a review matching an id
  """
  @abstractmethod
  def update_review(self, id):
    pass


  """
  Delete a review matching an id
  """
  @abstractmethod
  def delete_review(self, id):
    pass


