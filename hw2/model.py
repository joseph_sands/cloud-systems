from abstract import BaseModel

review_list = [["winter", "2018", "cs410", "feng", "great class"],["spring", "2018", "cs163", "fant", "difficult but worth it"],
               ["spring", "2018", "cs162", "fant", "not enjoyable"],["spring", "2018", "cs161", "black", "python is cool"]]

class DModel(BaseModel):
  
  def __init__(self, app):
    self.app = app

  def fetchall(self):
    return review_list

  def fetch_by_term(self, term):
    return True

  def fetch_by_instructor(self, instructor):
    return True

  def fetch_by_rating(self, rating):
    return True

  def fetch_by_id(self, id):
    return True

  def add_review(self, id):
    return True

  def update_review(self, id):
    return True

  def delete_review(self, id):
    return True
