# Joseph Sands - Cloud Systems Spring 2018 - HW2

from flask import Flask, render_template, redirect, request, url_for
import sqlite3
import flask.views
import requests

from model import DModel

app = Flask(__name__)
DB_FILE = 'reviews.db'

def select():
  """
  Retrieves all reviews
  """
  connection = sqlite3.connect(DB_FILE)
  cursor = connection.cursor()
  cursor.execute("SELECT * FROM reviews")
  return cursor.fetchall()


def insert(term, year, name, instructor, review):
  """
  Inserts a new review into the database
  """
  connection = sqlite3.connect(DB_FILE)
  params = {'term':term, 'year':year, 'name':name, 'instructor':instructor, 'review':review}
  cursor = connection.cursor()
  cursor.execute("insert into reviews (term, year, name, instructor, review) VALUES (:term, :year, :name, :instructor, :review)", params)
  
  connection.commit()
  cursor.close()
 

@app.route('/')
def index(): 
  """
  Base page to insert into the review DB
  """
  return render_template("index.html")


@app.route('/submit', methods=['POST'])
def submit():
  """
  Processes an insetion and redirects back to index
  """
  insert(request.form['term'], request.form['year'], request.form['name'], request.form['instructor'], request.form['review'])
  return redirect(url_for('index'))


@app.route('/reviews')
def reviews():
  """
  Display all reviews
  """
  reviews = []
  for row in select():
    entry = [row[0], row[1], row[2], row[3], row[4]]
    reviews.append(entry)
  return render_template("reviews.html", reviews=reviews)

if __name__ == "__main__":
  
  # Make sure the database exists
  connection = sqlite3.connect(DB_FILE)
  cursor = connection.cursor()
  try:
    cursor.execute("select count(asdf) from reviews")
  except sqlite3.OperationalError:
    print("Exception was reached")
    # cursor.execute("drop table reviews")
    cursor.execute("create table if not exists reviews (term text, year text, name text, instructor text, review text)")
  cursor.close()

  app.run(host = '0.0.0.0', port = 8000, debug=True)
